FROM python:3.7 as builder

COPY requirements.txt ./
RUN pip wheel --wheel-dir /python_packages -r requirements.txt


FROM python:3.7-slim

ENV APP_HOME /app
WORKDIR $APP_HOME

COPY --from=builder  /python_packages /python_packages
RUN pip install --no-index --find-links /python_packages /python_packages/* && rm -rf /python_packages
COPY . ./
CMD exec gunicorn --bind :$PORT --workers 1 --threads 8 manage:app
