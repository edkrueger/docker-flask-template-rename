import pytest
from project import app


@pytest.fixture
def client():
    return app.app.test_client()
