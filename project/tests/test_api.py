import json


def test_hello(client):
    with client as c:
        rv = c.get("/")
        assert json.loads(rv.data)["hello"] == "world"
